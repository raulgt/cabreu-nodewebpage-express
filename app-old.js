const http = require('http');

const hostname = '127.0.0.1';
const port = 8080;

const server = http.createServer((req, res)=>{


    // res.statusCode = 200;
    // res.setHeader('Content-Type', 'application/json');

    // Esta linea es equivalente a las dos de arriba
    res.writeHead(200,'Content-Type', 'application/json');
  
     
    let salida = {
        nombre: 'Carlos',
        edad: 37,
        url: req.url
    }
   res.write(JSON.stringify(salida));
    res.end();

});

server.listen(port, hostname, () =>{
    console.log(`Server running at http://${hostname}:${port}/`);
})

