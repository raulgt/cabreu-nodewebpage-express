const express = require('express');
const app = express();

const hbs = require('hbs');
require('./hbs/helpers/helpers');

const port = process.env.PORT || 3000;

//global variables
let currentAnio = new Date().getFullYear();

//Default page
app.use(express.static(__dirname + '/public'));

//Express HBS engine
hbs.registerPartials(__dirname + '/views/partials');

//Usar
app.set('view engine', 'hbs');

// Get to home page
app.get('/', (req, res) => {

    res.render('home', {
        nombre: 'Carlos Abreu',
        title: 'Home',
        // anio: currentAnio,
        lenguajes: [{ nombre: 'JavaScript' }, { nombre: 'C#' }, { nombre: 'python' }]
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        nombre: 'Carlos Abreu',
        title: 'About',
        // anio: currentAnio
    });
});

app.listen(port, () => {
    console.log(`Escuchando peticiones en el puerto ${ port }`);
});





// app.use('/about',express.static(__dirname + '/public/about.html'));

// app.get('/data', (req, res)=>{
//     res.send('HOLA DATA...!!!');
// })

// app.get('/dataJson', (req, res)=>{
//     let salida = {
//         nombre: 'Carlos Raul Abreu',
//         edad: 37,
//         url: req.url
//     }
//     res.send(salida);
// })

